package main.java;

/**
 * Created by markeloff on 19.11.16.
 */
public class TextMessages {
    public static final String ABOUT_MSG="Привет, я Робот-Добродеятель!" +
            "\nЯ попробую решить твою проблему, главное доверься мне!" +
            "\nЧтобы начать работу со мной напиши /start";
    public static final String START_MSG="Для продолжения работы выбрете то, что Вы хотели бы сделать: оказать помощь или просить помощи?";
    public static final String WANT_TO_HELP="Хочу помочь!";
    public static final String NEED_A_HELP="Нужна помощь!";
}

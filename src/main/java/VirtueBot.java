package main.java;

import main.keyboard.CustomKeyboard;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.sql.*;

import static main.java.TextMessages.*;

public class VirtueBot extends TelegramLongPollingBot {

    public static void main(String[] args) {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new VirtueBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "markeloff86";
    }

    @Override
    public String getBotToken() {
        return "293232770:AAGFOwBG-dKIhdPhJdUr1n5MYwnvwtvhSpc";
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            switch (message.getText()) {
                case "/help":
                    sendMsg(message, ABOUT_MSG);
                    break;
                case "/start":
                    sendMsg(message, START_MSG);
                case "Хочу помочь!":
                    sendMsg(message, WANT_TO_HELP);
                    break;
                case "Нужна помощь!":
                    sendMsg(message, NEED_A_HELP);
                    break;
                default:
                    sendMsg(message, "Я не знаю, что ответить на это");
                    break;
            }
        }
    }


    private void sendMsg(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton("Хочу помочь!"));
        keyboardRow.add(new KeyboardButton("Нужна помощь!"));
        new CustomKeyboard().clickCustomKeyboard(sendMessage, keyboardRow);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}